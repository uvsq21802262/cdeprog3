
/**
 * Write a description of class ScenarioMain here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class ScenarioMain
{
    
    public static void main(String[] args){
        Client c1 = new Client("c1");
        Client c2 = new Client("c2");
        Client c3 = new Client("c3");
        Server s = new Server();
        c1.connect(s);
        c2.connect(s);
        s.connect(c1);
        s.connect(c2);
        
        c3.connect(s);
        s.connect(c3);
        
        
        s.diffuseMsg(c3.sendMsg("Bonjour"));
        
        
        
   }
}
