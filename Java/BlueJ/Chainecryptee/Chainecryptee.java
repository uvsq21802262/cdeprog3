
/**
 * Décrivez votre classe Chainecryptee ici.
 *
 * @author (votre nom)
 * @version (un numéro de version ou une date)
 */
public class Chainecryptee implements ChaineInterface
{
    // variables d'instance - remplacez l'exemple qui suit par le vôtre
    private String stored;
    private int decalage;

    
    private Chainecryptee(String stored, int decalage){
        if(stored.length()==0){
        System.err.println("No chaine");
        System.exit(1);}
        this.stored = stored;
        this.decalage = decalage;
    }
    
    public static Chainecryptee deCryptee(String cryptee, int decalage){
        return new Chainecryptee(cryptee,decalage); 
    }
    
    public static Chainecryptee deEnClair(String clair, int decalage){
        return new Chainecryptee(clair,decalage); 
    }

    public String decrypte(){
        char[] chs = stored.toCharArray();
        for(int i=0;i<stored.length();i++){
            chs[i] = (char)(((chs[i] - 97 + 26 - decalage) % 26)+ 97);
        }
        return new String(chs);
    }
    
    /**
 * Décale un caractère majuscule.
 * Les lettres en majuscule ('A' - 'Z') sont décalés de <code>decalage</code>
 * caractères de façon circulaire. Les autres caractères ne sont pas modifiés.
 *
 * @param c le caractère à décaler
 * @param decalage le décalage à appliquer
 * @return le caractère décalé
 */
private static char decaleCaractere(char c, int decalage) {
    return (c < 'A' || c > 'Z')? c : (char)(((c - 'A' + decalage) % 26) + 'A');
}

    public String crypte(String clair){
        char[] chs = clair.toCharArray();
        for(int i=0;i<clair.length();i++){
            chs[i] = decaleCaractere(chs[i],decalage);
        }
        this.stored = new String(chs);
        return stored;
    }
    
}
