package fr.uvsq;

public class OverdraftException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8630924681220572259L;

	public String toString(){
		return "Operation aborted : the account risks being overdraft.";
	}
}
