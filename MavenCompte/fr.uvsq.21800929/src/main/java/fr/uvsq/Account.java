package fr.uvsq;
public class Account {
	private Balance balance;
	private static int cpt = 0;
	private int id;
	
	/**
	 * Initiation of the account
	 * @param amount : initial balance 
	 * @throws IllegalAmountException if amount is negative
	 */
	public Account(int amount) throws IllegalAmountException {
		balance = new Balance(amount);
		id = cpt;
		cpt++;
	}
	
	//return needs to be deleted if no value
	/**
	 * Credits an amount of money
	 * @param d : amount of money to add to the account
	 * @throws IllegalAmountException if d is negative
	 */
	public void deposit(int d) throws IllegalAmountException{//credit
		balance.addMoney(d);
	}
	
	
	/**
	 * Subtracts an amount of money
	 * @param d : amount of money to subtract to the account
	 * @throws IllegalAmountException : the amount to subtract can't be negative
	 * @throws OverdraftException : the account can't have a negative balance
	 */
	public void withdraw(int d) throws IllegalAmountException, OverdraftException {
		balance.subtractMoney(d);
	}
	
	
	/**
	 * Transfers money from this account to another one
	 * @param destination : account to transfer money to
	 * @param d : amount of money to transfer
	 * @throws NonexistentAccountException if destination is null
	 * @throws IllegalAmountException if d is negative
	 * @throws OverdraftException if this account risks being overdraft
	 */
	public void remit(Account destination, int d) throws NonexistentAccountException, IllegalAmountException, OverdraftException  {
		balance.subtractMoney(d);
		if (destination == null) throw new NonexistentAccountException();
			destination.balance.addMoney(d);
	}
	
	
	/**
	 * @return the amount of money in the account
	 */
	public int consult() {
		return balance.getNumber();
	}
	
	/**
	 * @return the unique id number of the account
	 */
	public int getId(){
		return id;
	}
	
}
