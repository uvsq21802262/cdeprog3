package fr.uvsq;

public class IllegalAmountException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -793400866456875592L;

	public String toString() {
		return "Illegal Amount of Money.";
	}
}
