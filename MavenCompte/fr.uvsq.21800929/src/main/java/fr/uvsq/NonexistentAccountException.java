package fr.uvsq;

public class NonexistentAccountException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String toString(){
		return "Operation on an account that does not exist.";
	}
}
