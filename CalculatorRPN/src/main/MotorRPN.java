package main;
import java.util.Stack;

/**
 * Contains the stack of operands and carry out the operations on its elements
 */
public class MotorRPN {

	/**
	 * Stack which contains the operands
	 */
	private Stack<Double> stackOperand = new Stack<Double>();
	
	/**
	 * Adds a new element at the top of the stack of the operands
	 * @param operand : the double to add to the stack
	 */
	public void save(double operand) {
		stackOperand.add(new Double(operand));
	}	
	
	
	/**
	 * Computes the operation on the first two operands of the stack
	 * @param o : the operation to operate
	 * @return : the result of the operation on the first two elements of the stack of operands
	 * @throws LackOperandException if there are less than two elements in the stack of operands
	 * @throws ArithmeticException if a division by zero is requested
	 */
	public double operate(Operation o) throws LackOperandException, ArithmeticException {
		if(stackOperand.size() < 2) throw new LackOperandException();
		double result = o.eval(stackOperand.pop(), stackOperand.pop());
		System.out.println(result);
		stackOperand.push(result);
		return result;
	}
	
	
	public String toString(){
		StringBuilder sb = new StringBuilder();
		for(Double d : stackOperand) {
			sb.append(d + " ");
		}
		return sb.toString();
	}
	
}
