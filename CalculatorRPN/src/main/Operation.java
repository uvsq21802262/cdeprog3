package main;
/**
 * Operations authorized in the calculator
 */
public enum Operation {
	
	PLUS ('+') {
		@Override
		protected double eval(double operand1 , double operand2) {
			System.out.print(operand2 + " + "+operand1 + " = ");
			return operand1 + operand2;
		}
	},
	
	MOINS ('-') {
		@Override
		protected double eval(double operand1 , double operand2) {
			System.out.print(operand2 + " - "+operand1 + " = ");
			return operand2 - operand1;
		}
	},
	
	MULT ('*') {
		@Override
		protected double eval(double operand1, double operand2) {
			System.out.print(operand2 + " * "+operand1 + " = ");
			return operand1 * operand2;
		}
	},
	
	DIV ('/') {
		@Override
		protected double eval(double operand1, double operand2) throws ArithmeticException {
			System.out.print(operand2 + " / "+operand1 + " = ");
			if(operand1 ==0 ) {
				throw new ArithmeticException();
			}
			return operand2 / operand1;
		}
	};
	
	private Operation(char symbol){
		this.symbol = symbol;
	}
	
	@SuppressWarnings("unused")
	private final char symbol;
	
	protected abstract double eval(double operand1, double operand2) throws ArithmeticException;

}
