package main;

/**
 * If there are less than two elements in the stack of operands in the MotorRPN class 
 * and an operator is input in the terminal, a LackOperandException is thrown
 */
public class LackOperandException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6475502316652827430L;

	public String toString() {
		return "Lack of operands";
	}
}
