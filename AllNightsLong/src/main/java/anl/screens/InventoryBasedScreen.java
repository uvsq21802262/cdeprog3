package anl.screens;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

import anl.ai.Creature;
import anl.item.Food;
import anl.item.Item;
import asciiPanel.AsciiPanel;

public abstract class InventoryBasedScreen implements Screen {

	protected Creature player;
	private String letters;
	protected AsciiPanel asciiPanel;
	protected final int x = 4;
	protected final int y = 23;
	protected abstract boolean isAcceptable(Item item);
	protected abstract Screen use(Item item);
	
	public InventoryBasedScreen(Creature player){
		this.player = player;
		this.letters = "abcdefghijklmnopqrstuvwxyz"; // according to BACKPACK_VOLUME
	}
	
	public void displayOutput(AsciiPanel terminal) {// shows the subscreen
		this.asciiPanel = terminal;
		ArrayList<String> lines = getList();
		
		//y = 23 - lines.size();
		//x = 4;

		if (lines.size() > 0)
			terminal.clear(' ', x, y - lines.size(), 20, lines.size());
		int yy = 0;
		for (String line : lines){
			terminal.write(line, x, y - lines.size()+ yy);
			yy++;
		}
		
		terminal.clear(' ', 0, 23, 80, 1);
		
		String verb = (player.DONATING == true? "give":"use");
		terminal.write("What would you like to "+verb +"?", 2, 23);
		
		terminal.repaint();
	}
	
	protected ArrayList<String> getList() {
		ArrayList<String> lines = new ArrayList<String>();
		Item[] inventory = player.inventory().getItems();
		
		for (int i = 0; i < inventory.length; i++){
			Item item = inventory[i];
			
			if (item == null || !isAcceptable(item))
				continue;
			
			String line = letters.charAt(i) + " - " + item.glyph() + " " + player.nameOf(item);
			
			if(item == player.weapon() || item == player.armor())
				line += " (equipped)";
			
			lines.add(line);
		}
		return lines;
	}

	public Screen respondToUserInput(KeyEvent key) {
		char c = key.getKeyChar();

		Item[] items = player.inventory().getItems();

		if (letters.indexOf(c) > -1 
				&& items.length > letters.indexOf(c)
				&& items[letters.indexOf(c)] != null
				&& isAcceptable(items[letters.indexOf(c)])) {
			return use(items[letters.indexOf(c)]);
		} else if (key.getKeyCode() == KeyEvent.VK_ESCAPE) {
			if(player.DONATING == true) {
				player.notify("My condition isn't good. Maybe next time.");
				player.DONATING = false;
				player.setEmote(-10);
			}
			return null;
		}
		else {
			return this;
		}
	}
	

}
