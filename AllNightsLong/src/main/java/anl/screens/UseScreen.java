package anl.screens;

import java.awt.event.KeyEvent;
import java.util.ArrayList;

import anl.ai.Creature;
import anl.item.Armor;
import anl.item.Food;
import anl.item.Item;
import anl.item.Medicine;
import anl.item.Weapon;
import asciiPanel.AsciiPanel;

public class UseScreen extends InventoryBasedScreen implements Screen {

	private Item item;
	public UseScreen(Creature player, Item item) {
		super(player);
		this.item = item;
	}

	@Override
	protected boolean isAcceptable(Item item) {
		if(item instanceof Weapon) return ((Weapon)item).quaffEffect() != null;
		if(item instanceof Armor) return ((Armor)item).quaffEffect() != null;
		return true;
	}

	@Override
	protected Screen use(Item item) {
		if(item instanceof Food) {
			player.eat((Food) item);
		}else if(item instanceof Weapon) {
			if(player.DONATING == true) player.notify("He doesn't need it.");
			else {
				player.notify("Try to use the "+item.name()+" as weapon.");
				player.equip((Weapon) item);
			}
		}else if(item instanceof Armor) {
			if(player.DONATING == true) player.notify("He doesn't need it.");
			else {
				player.notify("Try to wear the "+item.name());
				player.equip((Armor) item);
			}
		}else if(item instanceof Medicine) {
			player.eat((Medicine) item);
		}	
		return null;
	}
	

	protected Screen drop(Item item) { 
		player.drop(item); 
		return null;
	}
	
	@Override
	public Screen respondToUserInput(KeyEvent key) {
	if(key.getKeyCode()==KeyEvent.VK_U) {
		return use(item); 
	}else if(key.getKeyCode() == KeyEvent.VK_D) {
		return drop(item);
	}else{
		return null;
	}
}
	public void displayOutput(AsciiPanel terminal) {
		terminal.clear(' ', 0, 22, 80, 2);
		String article = "aeiou".contains(player.nameOf(item).subSequence(0, 1)) ? "an " : "a ";
		terminal.write("It's " + article + player.nameOf(item) + "." + item.details(), 2, 22);
		terminal.write("Do you want to use it(u) or drop it(d) or just check it out(esc)?", 2, 23);
		terminal.repaint();
	}

}
