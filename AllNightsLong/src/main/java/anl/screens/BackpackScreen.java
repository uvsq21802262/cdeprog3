package anl.screens;

import java.awt.event.KeyEvent;

import anl.ai.Creature;
import anl.item.Armor;
import anl.item.Food;
import anl.item.Item;
import anl.item.Medicine;
import anl.item.Weapon;

public class BackpackScreen extends InventoryBasedScreen implements Screen {

	public BackpackScreen(Creature player) {
		super(player);
	}

	@Override
	protected boolean isAcceptable(Item item) {
		if(item instanceof Weapon) return ((Weapon)item).attackValue() >= 0;
		else if( item instanceof Armor) return ((Armor)item).defenseValue() > 0;
		else if( item instanceof Food) return ((Food)item).foodValue() > 0;
		else if( item instanceof Medicine) return ((Medicine) item).getCure() > 0;
		else return false;
	}

	@Override
	protected Screen use(Item item) {
		Screen subScreen = new UseScreen(player, item);
		displayOutput(asciiPanel);
		return subScreen;
	}	
	
	

}
