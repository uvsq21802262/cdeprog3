package anl.screens;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import anl.FieldOfView;
import anl.StuffFactory;
import anl.Tile;
import anl.World;
import anl.WorldBuilder;
import anl.ai.Creature;
import anl.item.Item;
import asciiPanel.AsciiPanel;
import anl.outil.Constants;
public class PlayScreen implements Screen {
	private World world;
	private Creature player;
	private int screenWidth;
	private int screenHeight;
	private List<String> messages;
	private FieldOfView fov;
	private Screen subscreen;//updates
	
	public PlayScreen(){
		screenWidth = 80;
		screenHeight = 23;
		messages = new ArrayList<String>();
		createWorld();
		fov = new FieldOfView(world);
		
		StuffFactory factory = new StuffFactory(world);
		createCreatures(factory);
		createItems(factory);
	}

	private void createCreatures(StuffFactory factory){
		player = factory.newPlayer( messages, fov);
		
		for (int z = 0; z < world.depth(); z++){
//			for (int i = 0; i < 4; i++){
//				factory.newFungus(z);
//			}
//			for (int i = 0; i < 10; i++){
//				factory.newBat(z);
//			}
//			for (int i = 0; i < z * 2 + 1; i++){
//				factory.newZombie(z, player);
//				factory.newGoblin(z, player);
//			}
			for(int i=0; i< Constants.NUM_SOLDIER;i++) {
				factory.newSoldier(z, messages, fov);
			}
			
			for(int i=0; i< Constants.NUM_PERSON;i++) {
				factory.newPerson(z, messages, fov);
			}
			
			for(int i=0; i< Constants.NUM_REFUGEE;i++) {
				factory.newRefugee(z, messages, fov);
			}
		}
	}

	private void createItems(StuffFactory factory) {
		for (int z = 0; z < world.depth(); z++){
			for (int i = 0; i < world.width() * world.height() / 50; i++){
				factory.newRock(z);
			}

			factory.newFruit(z);
			//factory.newEdibleWeapon(z);
			factory.newBread(z);
			factory.randomArmor(z);
			factory.randomWeapon(z);
			factory.randomWeapon(z);
			factory.newRemedy(z);
			factory.newDrug(z);
			factory.newDrug(z);
			
//			for (int i = 0; i < z + 1; i++){
//				factory.randomPotion(z);
//				factory.randomSpellBook(z);
//			}
		}
//		factory.newVictoryItem(world.depth() - 1);
	}
	
	private void createWorld(){
		world = new WorldBuilder(90, 32, 5)
					.makeCaves()
					.build();
	}
	
	public int getScrollX() { return Math.max(0, Math.min(player.x - screenWidth / 2, world.width() - screenWidth)); }
	
	public int getScrollY() { return Math.max(0, Math.min(player.y - screenHeight / 2, world.height() - screenHeight)); }
	

	public void displayOutput(AsciiPanel terminal) {
		int left = getScrollX();
		int top = getScrollY(); 
		
		displayTiles(terminal, left, top);
		displayMessages(terminal, messages);
		
		//String stats = String.format(" Attack %d Defense %d %8s %8s %8s", player.attackValue(), player.defenseValue(), emotes(), hunger(), injure());
		String stats = "";
		stats += " Attack "+player.attackValue()+ " Defense "+player.defenseValue()+"   ";
		terminal.write(stats, 1, 23);
		if(emotes().equals("Grieve")||emotes().equals("Sorrow")) terminal.write(emotes(), 1+stats.length()+2, 23, AsciiPanel.red);
		else terminal.write(emotes(), 1+stats.length()+2, 23);
		stats+=emotes()+"   ";
		if(hunger().equals("Starving")||hunger().equals("Hungry")) terminal.write(hunger(), 1+stats.length()+2, 23, AsciiPanel.red);
		else terminal.write(hunger(), 1+stats.length()+2, 23);
		stats+=hunger()+"   ";
		if(injure().equals("Severely injured")||injure().equals("Deadly")) terminal.write(injure(), 1+stats.length()+2, 23, AsciiPanel.red);
		else terminal.write(injure(), 1+stats.length()+2, 23);
		stats+=injure();
		
		if(player.DONATING) terminal.write("DONATING", 1+stats.length()+2, 23);
		
		if (subscreen != null)
			subscreen.displayOutput(terminal);
	}
	
	

	private String emotes() {
		double emoteIndex = (double)player.food()/player.maxFood() * 0.2 + (double)player.hp() / player.maxHp()*0.3 + player.getEmote()/100*0.5; 
		//System.out.println(emoteIndex);
		if(emoteIndex < 0.2) {
			if(emoteIndex <= 0) player.modifyHp(-100, Constants.DEATH_MENTAL);
			return "Grieve";
			}
		if(emoteIndex < 0.4)
			return "Sorrow";
		if(emoteIndex < 0.6)
			return "Grumble";
		if(emoteIndex < 0.8)
			return "Irritable";
		return "Happy";
	}

	private String hunger(){
		if (player.food() < player.maxFood() * 0.10)
			return "Starving";
		else if (player.food() < player.maxFood() * 0.25)
			return "Hungry";
		else if (player.food() > player.maxFood() * 0.90)
			return "Stuffed";
		else if (player.food() > player.maxFood() * 0.75)
			return "Full";
		else
			return "";
	}
	
	private String injure(){
		if (player.hp() < player.maxHp() * 0.90)
			return "Hurt";
		else if (player.hp() < player.maxHp() * 0.75)
			return "Injured";
		else if (player.hp() < player.maxHp() * 0.35)
			return "Severely Injured";
		else if (player.hp() < player.maxHp() * 0.10)
			return "Deadly";
		else
			return "";
	}

	private void displayMessages(AsciiPanel terminal, List<String> messages) {
		int top = screenHeight - messages.size();
		for (int i = 0; i < messages.size(); i++){
			//System.out.println(top);
			//if(top<0||top>=24) continue;
			terminal.writeCenter(messages.get(i).toString(), top + i);
		}
		if (subscreen == null)
			messages.clear();
	}

	private void displayTiles(AsciiPanel terminal, int left, int top) {
		fov.update(player.x, player.y, player.z, player.visionRadius());
		
		for (int x = 0; x < screenWidth; x++){
			for (int y = 0; y < screenHeight; y++){
				int wx = x + left;
				int wy = y + top;

				if (player.canSee(wx, wy, player.z))
					terminal.write(world.glyph(wx, wy, player.z), x, y, world.color(wx, wy, player.z));
				else
					terminal.write(fov.tile(wx, wy, player.z).glyph(), x, y, Color.darkGray);
			}
		}
	}
	

	public Screen respondToUserInput(KeyEvent key) {
		//int level = player.level();
		
		if (subscreen != null) {
			subscreen = subscreen.respondToUserInput(key);
		} else {
			switch (key.getKeyCode()){
			case KeyEvent.VK_LEFT:
			case KeyEvent.VK_H: player.moveBy(-1, 0, 0); break;
			case KeyEvent.VK_RIGHT:
			case KeyEvent.VK_L: player.moveBy( 1, 0, 0); break;
			case KeyEvent.VK_UP:
			case KeyEvent.VK_K: player.moveBy( 0,-1, 0); break;
			case KeyEvent.VK_DOWN:
			case KeyEvent.VK_J: player.moveBy( 0, 1, 0); break;
			case KeyEvent.VK_Y: player.moveBy(-1,-1, 0); break;
			case KeyEvent.VK_U: player.moveBy( 1,-1, 0); break;
			case KeyEvent.VK_B: player.moveBy(-1, 1, 0); break;
			case KeyEvent.VK_N: player.moveBy( 1, 1, 0); break;
			case KeyEvent.VK_A: player.playerAttack(); break;
			case KeyEvent.VK_I: subscreen = new BackpackScreen(player); break;
			case KeyEvent.VK_SEMICOLON: subscreen = new LookScreen(player, "Looking", 
					player.x - getScrollX(), 
					player.y - getScrollY()); break;
			case KeyEvent.VK_T: subscreen = new ThrowScreen(player,
					player.x - getScrollX(), 
					player.y - getScrollY()); break;
			case KeyEvent.VK_F: 
				if (player.weapon() == null || player.weapon().rangedAttackValue() == 0)
					player.notify("You don't have a ranged weapon equiped.");
				else
					subscreen = new FireWeaponScreen(player,
						player.x - getScrollX(), 
						player.y - getScrollY()); break;
			case KeyEvent.VK_R: subscreen = new ReadScreen(player,
						player.x - getScrollX(), 
						player.y - getScrollY()); break;
			}
			
			switch (key.getKeyChar()){
			case 'g':
			case ',': player.pickup(); break;
			case '<': 
				if (userIsTryingToExit())
					return userExits();
				else
					player.moveBy( 0, 0, -1); break;
			case '>': player.moveBy( 0, 0, 1); break;
			case '?': subscreen = new HelpScreen(); break;
			}
		}

//		if (player.level() > level)
//			subscreen = new LevelUpScreen(player, player.level() - level);
		
		if (subscreen == null)
			world.update();
		
		if (player.hp() < 1)
			return new LoseScreen(player);
		
		return this;
	}

	private boolean userIsTryingToExit(){
		return player.z == 0 && world.tile(player.x, player.y, player.z) == Tile.STAIRS_UP;
	}
	
	private Screen userExits(){
		for (Item item : player.inventory().getItems()){
			if (item != null && item.name().equals("teddy bear"))
				return new WinScreen();
		}
		player.modifyHp(0, "Died while cowardly fleeing the caves.");
		return new LoseScreen(player);
	}
}
