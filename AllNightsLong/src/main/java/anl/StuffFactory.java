package anl;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import anl.ai.BatAi;
import anl.ai.Creature;
import anl.ai.FungusAi;
import anl.ai.GoblinAi;
import anl.ai.PersonAi;
import anl.ai.PlayerAi;
import anl.ai.RefugeeAi;
import anl.ai.SoldierAi;
import anl.ai.ZombieAi;
import anl.item.Armor;
import anl.item.Bandage;
import anl.item.Drug;
import anl.item.Food;
import anl.item.Item;
import anl.item.Medicine;
import anl.item.Remedy;
import anl.item.Weapon;
import anl.outil.Constants;
import asciiPanel.AsciiPanel;

public class StuffFactory {
	private World world;
	private Map<String, Color> potionColors;
	private List<String> potionAppearances;
	
	public StuffFactory(World world){
		this.world = world;
		
		setUpPotionAppearances();
	}
	
	private void setUpPotionAppearances(){
		potionColors = new HashMap<String, Color>();
		potionColors.put("red potion", AsciiPanel.brightRed);
		potionColors.put("yellow potion", AsciiPanel.brightYellow);
		potionColors.put("green potion", AsciiPanel.brightGreen);
		potionColors.put("cyan potion", AsciiPanel.brightCyan);
		potionColors.put("blue potion", AsciiPanel.brightBlue);
		potionColors.put("magenta potion", AsciiPanel.brightMagenta);
		potionColors.put("dark potion", AsciiPanel.brightBlack);
		potionColors.put("grey potion", AsciiPanel.white);
		potionColors.put("light potion", AsciiPanel.brightWhite);

		potionAppearances = new ArrayList<String>(potionColors.keySet());
		Collections.shuffle(potionAppearances);
	}
	
	public Creature newPlayer(List<String> messages, FieldOfView fov){
		Creature player = new Creature(world, Constants.PLAYER, AsciiPanel.brightWhite, "player", 100, 20+(int)(Math.random()*10), 1+(int)(Math.random()*10));
		world.addAtEmptyLocation(player, 0);
		new PlayerAi(player, messages, fov);
		return player;
	}
	

	public Creature newSoldier(int depth, List<String> messages, FieldOfView fov) {
		Creature soldier = new Creature(world, Constants.SOLDIER, AsciiPanel.brightMagenta, "soldier", 100, 60+(int)(Math.random()*10), 40+(int)(Math.random()*10));
		world.addAtEmptyLocation(soldier, depth);
		new SoldierAi(soldier, messages, fov, this);
		return soldier;
	}
	
	public Creature newPerson(int depth, List<String> messages, FieldOfView fov) {
		Creature person = new Creature(world, Constants.PERSON, AsciiPanel.brightRed, "person", 100, 20+(int)(Math.random()*10), 1+(int)(Math.random()*10));
		world.addAtEmptyLocation(person, depth);
		new PersonAi(person, messages, fov, this);
		return person;
	}
	
	public Creature newRefugee(int depth, List<String> messages, FieldOfView fov) {
		Creature refugee = new Creature(world, Constants.REFUGEE, AsciiPanel.brightRed, "refugee", 100, 25+(int)(Math.random()*10), 10+(int)(Math.random()*10));
		world.addAtEmptyLocation(refugee, depth);
		new RefugeeAi(refugee, messages, fov, this);
		return refugee;
		
	}
	
	public Creature newFungus(int depth){
		Creature fungus = new Creature(world, 'f', AsciiPanel.green, "fungus", 10, 0, 0);
		world.addAtEmptyLocation(fungus, depth);
		new FungusAi(fungus, this);
		return fungus;
	}
	
	public Creature newBat(int depth){
		Creature bat = new Creature(world, 'b', AsciiPanel.brightYellow, "bat", 15, 5, 0);
		world.addAtEmptyLocation(bat, depth);
		new BatAi(bat);
		return bat;
	}
	
	public Creature newZombie(int depth, Creature player){
		Creature zombie = new Creature(world, 'z', AsciiPanel.white, "zombie", 50, 10, 10);
		world.addAtEmptyLocation(zombie, depth);
		new ZombieAi(zombie, player);
		return zombie;
	}

	public Creature newGoblin(int depth, Creature player){
		Creature goblin = new Creature(world, 'g', AsciiPanel.brightGreen, "goblin", 66, 15, 5);
		new GoblinAi(goblin, player);
//		goblin.equip(randomWeapon(depth));
//		goblin.equip(randomArmor(depth));
		world.addAtEmptyLocation(goblin, depth);
		return goblin;
	}
	
	
	
	public Weapon newRock(int depth){
		Weapon rock = new Weapon(Constants.ROCK, AsciiPanel.yellow, "rock", null);
		rock.modifyThrownAttackValue(Constants.ROCK_ATTACK_VALUE);
		world.addAtEmptyLocation(rock, depth);
		return rock;
	}
	
	public Item newVictoryItem(int depth){
		Item item = new Item('*', AsciiPanel.brightWhite, "teddy bear", null);
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Food newBread(int depth){
		Food item = new Food(Constants.FOOD, AsciiPanel.yellow, "bread", null, Constants.BREAD_VALUE);
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Food newCan(int depth){
		Food item = new Food(Constants.FOOD, AsciiPanel.brightBlack, "can", null, Constants.CAN_VALUE);
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Food newFruit(int depth){
		Food item = new Food(Constants.FOOD, AsciiPanel.brightRed, "apple", null, Constants.APPLE_VALUE);
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Weapon newDagger(int depth){
		Weapon item = new Weapon(Constants.WEAPON, AsciiPanel.white, "dagger", null);
		item.modifyAttackValue(Constants.DAGGER_ATTACK_VALUE);
		item.modifyThrownAttackValue(Constants.DAGGER_THROWN_ATTACK_VALUE);
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Weapon newStick(int depth){
		Weapon item = new Weapon(Constants.WEAPON, AsciiPanel.brightWhite, "stick", null);
		item.modifyAttackValue(Constants.STICK_ATTACK_VALUE);
		item.modifyThrownAttackValue(Constants.STICK_THROWN_ATTACK_VALUE);
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Weapon newGun(int depth){
		Weapon item = new Weapon(Constants.WEAPON, AsciiPanel.yellow, "gun", null);
		item.modifyAttackValue(Constants.GUN_ATTACK_VALUE);
		item.modifyRangedAttackValue(Constants.GUN_RANGE_ATTACK_VALUE);
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	
	public Armor newLightArmor(int depth){
		Armor item = new Armor(Constants.ARMOR, AsciiPanel.green, "body armor", null, Constants.ARMOR_DEFENSE_VALUE);
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Medicine newRemedy(int depth){
		Medicine item = new Remedy();
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Medicine newDrug(int depth){
		Medicine item = new Drug();
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	public Medicine newBandage(int depth){
		Medicine item = new Bandage();
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Weapon randomWeapon(int depth){
		switch ((int)(Math.random() * 10)){
		case 0: 
		case 1:
		case 2:
		case 3:
		case 4:
		case 5:
		case 6: return newDagger(depth); 
		case 7: 
		case 8: return newStick(depth); 
		case 9:
		default: return newGun(depth); 
		}
	}

	public Armor randomArmor(int depth){
		return newLightArmor(depth);
		
	}
	
	public Item newPotionOfHealth(int depth){
		String appearance = potionAppearances.get(0);
		final Item item = new Item('!', potionColors.get(appearance), "health potion", appearance);
//		item.setQuaffEffect(new Effect(1){
//			public void start(Creature creature){
//				if (creature.hp() == creature.maxHp())
//					return;
//				
//				creature.modifyHp(15, "Killed by a health potion?");
//				creature.doAction(item, "look healthier");
//			}
//		});
//		
//		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Item newPotionOfMana(int depth){
		String appearance = potionAppearances.get(1);
		final Item item = new Item('!', potionColors.get(appearance), "mana potion", appearance);
//		item.setQuaffEffect(new Effect(1){
//			public void start(Creature creature){
//				if (creature.mana() == creature.maxMana())
//					return;
//				
//				creature.modifyMana(10);
//				creature.doAction(item, "look restored");
//			}
//		});
		
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Item newPotionOfSlowHealth(int depth){
		String appearance = potionAppearances.get(2);
		final Item item = new Item('!', potionColors.get(appearance), "slow health potion", appearance);
//		item.setQuaffEffect(new Effect(100){
//			public void start(Creature creature){
//				creature.doAction(item, "look a little better");
//			}
//			
//			public void update(Creature creature){
//				super.update(creature);
//				creature.modifyHp(1, "Killed by a slow health potion?");
//			}
//		});
//		
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Item newPotionOfPoison(int depth){
		String appearance = potionAppearances.get(3);
		final Item item = new Item('!', potionColors.get(appearance), "poison potion", appearance);
//		item.setQuaffEffect(new Effect(20){
//			public void start(Creature creature){
//				creature.doAction(item, "look sick");
//			}
//			
//			public void update(Creature creature){
//				super.update(creature);
//				creature.modifyHp(-1, "Died of poison.");
//			}
//		});
		
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Item newPotionOfWarrior(int depth){
		String appearance = potionAppearances.get(4);
		final Item item = new Item('!', potionColors.get(appearance), "warrior's potion", appearance);
//		item.setQuaffEffect(new Effect(20){
//			public void start(Creature creature){
//				creature.modifyAttackValue(5);
//				creature.modifyDefenseValue(5);
//				creature.doAction(item, "look stronger");
//			}
//			public void end(Creature creature){
//				creature.modifyAttackValue(-5);
//				creature.modifyDefenseValue(-5);
//				creature.doAction("look less strong");
//			}
//		});
//		
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	public Item newPotionOfArcher(int depth){
		String appearance = potionAppearances.get(5);
		final Item item = new Item('!', potionColors.get(appearance), "archers potion", appearance);
//		item.setQuaffEffect(new Effect(20){
//			public void start(Creature creature){
//				creature.modifyVisionRadius(3);
//				creature.doAction(item, "look more alert");
//			}
//			public void end(Creature creature){
//				creature.modifyVisionRadius(-3);
//				creature.doAction("look less alert");
//			}
//		});
		
		world.addAtEmptyLocation(item, depth);
		return item;
	}

	public Item newPotionOfExperience(int depth){
		String appearance = potionAppearances.get(6);
		final Item item = new Item('!', potionColors.get(appearance), "experience potion", appearance);
//		item.setQuaffEffect(new Effect(20){
//			public void start(Creature creature){
//				creature.doAction(item, "look more experienced");
//				creature.modifyXp(creature.level() * 5);
//			}
//		});
//		
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Item randomPotion(int depth){
		switch ((int)(Math.random() * 9)){
		case 0: return newPotionOfHealth(depth);
		case 1: return newPotionOfHealth(depth);
		case 2: return newPotionOfMana(depth);
		case 3: return newPotionOfMana(depth);
		case 4: return newPotionOfSlowHealth(depth);
		case 5: return newPotionOfPoison(depth);
		case 6: return newPotionOfWarrior(depth);
		case 7: return newPotionOfArcher(depth);
		default: return newPotionOfExperience(depth);
		}
	}
	
	public Item newWhiteMagesSpellbook(int depth) {
		Item item = new Item('+', AsciiPanel.brightWhite, "white mage's spellbook", null);
//		item.addWrittenSpell("minor heal", 4, new Effect(1){
//			public void start(Creature creature){
//				if (creature.hp() == creature.maxHp())
//					return;
//				
//				creature.modifyHp(20, "Killed by a minor heal spell?");
//				creature.doAction("look healthier");
//			}
//		});
//		
//		item.addWrittenSpell("major heal", 8, new Effect(1){
//			public void start(Creature creature){
//				if (creature.hp() == creature.maxHp())
//					return;
//				
//				creature.modifyHp(50, "Killed by a major heal spell?");
//				creature.doAction("look healthier");
//			}
//		});
//		
//		item.addWrittenSpell("slow heal", 12, new Effect(50){
//			public void update(Creature creature){
//				super.update(creature);
//				creature.modifyHp(2, "Killed by a slow heal spell?");
//			}
//		});
//
//		item.addWrittenSpell("inner strength", 16, new Effect(50){
//			public void start(Creature creature){
//				creature.modifyAttackValue(2);
//				creature.modifyDefenseValue(2);
//				creature.modifyVisionRadius(1);
//				creature.modifyRegenHpPer1000(10);
//				creature.modifyRegenManaPer1000(-10);
//				creature.doAction("seem to glow with inner strength");
//			}
//			public void update(Creature creature){
//				super.update(creature);
//				if (Math.random() < 0.25)
//					creature.modifyHp(1, "Killed by inner strength spell?");
//			}
//			public void end(Creature creature){
//				creature.modifyAttackValue(-2);
//				creature.modifyDefenseValue(-2);
//				creature.modifyVisionRadius(-1);
//				creature.modifyRegenHpPer1000(-10);
//				creature.modifyRegenManaPer1000(10);
//			}
//		});
		
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	
	public Item newBlueMagesSpellbook(int depth) {
		Item item = new Item('+', AsciiPanel.brightBlue, "blue mage's spellbook", null);

//		item.addWrittenSpell("blood to mana", 1, new Effect(1){
//			public void start(Creature creature){
//				int amount = Math.min(creature.hp() - 1, creature.maxMana() - creature.mana());
//				creature.modifyHp(-amount, "Killed by a blood to mana spell.");
//				creature.modifyMana(amount);
//			}
//		});
//		
//		item.addWrittenSpell("blink", 6, new Effect(1){
//			public void start(Creature creature){
//				creature.doAction("fade out");
//				
//				int mx = 0;
//				int my = 0;
//				
//				do
//				{
//					mx = (int)(Math.random() * 11) - 5;
//					my = (int)(Math.random() * 11) - 5;
//				}
//				while (!creature.canEnter(creature.x+mx, creature.y+my, creature.z)
//						&& creature.canSee(creature.x+mx, creature.y+my, creature.z));
//				
//				creature.moveBy(mx, my, 0);
//				
//				creature.doAction("fade in");
//			}
//		});
//		
//		item.addWrittenSpell("summon bats", 11, new Effect(1){
//			public void start(Creature creature){
//				for (int ox = -1; ox < 2; ox++){
//					for (int oy = -1; oy < 2; oy++){
//						int nx = creature.x + ox;
//						int ny = creature.y + oy;
//						if (ox == 0 && oy == 0 
//								|| creature.creature(nx, ny, creature.z) != null)
//							continue;
//						
//						Creature bat = newBat(0);
//						
//						if (!bat.canEnter(nx, ny, creature.z)){
//							world.remove(bat);
//							continue;
//						}
//						
//						bat.x = nx;
//						bat.y = ny;
//						bat.z = creature.z;
//						
//						creature.summon(bat);
//					}
//				}
//			}
//		});
//		
//		item.addWrittenSpell("detect creatures", 16, new Effect(75){
//			public void start(Creature creature){
//				creature.doAction("look far off into the distance");
//				creature.modifyDetectCreatures(1);
//			}
//			public void end(Creature creature){
//				creature.modifyDetectCreatures(-1);
//			}
//		});
		world.addAtEmptyLocation(item, depth);
		return item;
	}
	

	public Item randomSpellBook(int depth){
		switch ((int)(Math.random() * 2)){
		case 0: return newWhiteMagesSpellbook(depth);
		default: return newBlueMagesSpellbook(depth);
		}
	}

	

	
}
