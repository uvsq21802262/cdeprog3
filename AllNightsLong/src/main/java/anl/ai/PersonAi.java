package anl.ai;

import java.util.List;

import anl.FieldOfView;
import anl.StuffFactory;
import anl.Tile;
import anl.item.Item;
import anl.outil.Constants;

public class PersonAi extends CreatureAi {

	protected StuffFactory factory;
	protected int spreadcount;
	protected List<String> messages;
	protected FieldOfView fov;


	
	public PersonAi(Creature creature, List<String> messages, FieldOfView fov, StuffFactory factory) {
		super(creature);
		this.messages = messages;
		this.fov = fov;
		this.factory = factory;
		attackSuccessRatio = 0.25;
		
	}
		
		@Override
		public void attack() {
			if(canAttack(player)) {
				this.creature.meleeAttack(player);
				player.notify(Constants.UNDER_ATTACK);
			}
		}
		
		public void onUpdate(){
			if (spreadcount < 10 && Math.random() < 0.01)
				flock();
			if(attackStatus&&player!=null&&player.hp()>0) {
				if(HUNTING==false) HUNTING=true;
				
				else if (creature.canSee(player.x, player.y, player.z)&&hunt(player)) {
//					if (canRangedWeaponAttack(player))
//						creature.rangedWeaponAttack(player);// that is way too dangerous
////					else if (canThrowAt(player))
////						creature.throwItem(getWeaponToThrow(), player.x, player.y, player.z);
//					else if(canAttack(player))
//						creature.meleeAttack(player);
//					else {
//					
//					}
				}else {
					if(underAttackStatus) return;
					wander();
				}
			}else if(underAttackStatus&&player!=null&&player.hp()>0) {
				this.attack();
				attackStatus = true;
			}
			else wander();
		}

		protected boolean canAttack(Creature player) {
			if(this.creature.getNeighbour8().contains(player)) return true;
			else return false;
		}


		protected void flock(){
			int x = creature.x + (int)(Math.random() * 20) - 10;
			int y = creature.y + (int)(Math.random() * 19) - 10;
			
			if (!creature.canEnter(x, y, creature.z))
				return;
			
			Creature child = factory.newSoldier(creature.z, messages, fov);
			child.x = x;
			child.y = y;
			child.z = creature.z;
			spreadcount++;
		}
		
		

		public void onEnter(int x, int y, int z, Tile tile){
			if (tile.isGround()){
				creature.x = x;
				creature.y = y;
				creature.z = z;
				
				Item item = creature.item(creature.x, creature.y, creature.z);
				if (item != null)
					creature.pickup();
				
			} else if (tile.isDiggable()) {
				creature.dig(x, y, z);
			}
		}
		
		public void onNotify(String message){
			messages.add(message);
		}
		
		public boolean canSee(int wx, int wy, int wz) {
			return fov.isVisible(wx, wy, wz);
		}
		
		public void onGainLevel(){
		}

		public Tile rememberedTile(int wx, int wy, int wz) {
			return fov.tile(wx, wy, wz);
		}
	}