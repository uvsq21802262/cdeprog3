package anl.ai;

import java.util.List;

import anl.FieldOfView;
import anl.StuffFactory;
import anl.Tile;
import anl.item.Item;
import anl.outil.Constants;

public class SoldierAi extends PersonAi {
	
	public SoldierAi(Creature creature, List<String> messages, FieldOfView fov, StuffFactory factory) {
		super(creature,messages,fov,factory);
		attackSuccessRatio = 0.85;
	}
	
	public void onUpdate(){
		if (spreadcount < 10 && Math.random() < 0.01)
			flock();
		if(attackStatus&&player!=null&&player.hp()>0) {
			if(HUNTING==false) HUNTING=true;
			
			else if (creature.canSee(player.x, player.y, player.z)&&hunt(player)) {
//				if (canRangedWeaponAttack(player))
//					creature.rangedWeaponAttack(player);// that is way too dangerous
////				else if (canThrowAt(player))
////					creature.throwItem(getWeaponToThrow(), player.x, player.y, player.z);
//				else if(canAttack(player))
//					creature.meleeAttack(player);
//				else {
//				
//				}
			}else {
				if(underAttackStatus) return;
				wander();
			}
		}else if(underAttackStatus&&player!=null&&player.hp()>0) {
			this.attack();
			attackStatus = true;
		}
		else wander();
	}


	protected void flock(){
		int x = creature.x + (int)(Math.random() * 20) - 10;
		int y = creature.y + (int)(Math.random() * 19) - 10;
		
		if (!creature.canEnter(x, y, creature.z))
			return;
		
		Creature child = factory.newSoldier(creature.z, messages, fov);
		child.x = x;
		child.y = y;
		child.z = creature.z;
		spreadcount++;
	}

	public void snipher() {
		player.modifyHp(-80, Constants.DEATH_SNIPHER);
	}
}
