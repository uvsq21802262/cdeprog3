package anl.ai;

import java.util.List;

import anl.FieldOfView;
import anl.StuffFactory;

public class RefugeeAi extends PersonAi {

	public RefugeeAi(Creature creature, List<String> messages, FieldOfView fov, StuffFactory factory) {
		super(creature, messages, fov, factory);
		attackSuccessRatio = 0.4;
	}

	public void onUpdate(){
		if (spreadcount < 50 && Math.random() < 0.04)
			flock();
		if(player!=null&&player.DONATING == true) return;
		
		if(attackStatus&&player!=null&&player.hp()>0) {
			if(HUNTING==false) HUNTING=true;
			
			else if (creature.canSee(player.x, player.y, player.z)&&hunt(player)) {
//				if (canRangedWeaponAttack(player))
//					creature.rangedWeaponAttack(player);// that is way too dangerous
////				else if (canThrowAt(player))
////					creature.throwItem(getWeaponToThrow(), player.x, player.y, player.z);
//				else if(canAttack(player))
//					creature.meleeAttack(player);
//				else {
//				
//				}
			}else {
				if(underAttackStatus) return;
				wander();
			}
		}else if(underAttackStatus&&player!=null&&player.hp()>0) {
			this.attack();
			attackStatus = true;
		}
		else wander();
	}
	
	@Override
	protected void flock(){
		int x = creature.x + (int)(Math.random() * 10) - 5;
		int y = creature.y + (int)(Math.random() * 19) - 5;
		
		if (!creature.canEnter(x, y, creature.z))
			return;
		
		Creature child = factory.newRefugee(creature.z, messages, fov);
		child.x = x;
		child.y = y;
		child.z = creature.z;
		spreadcount++;
	}
}
