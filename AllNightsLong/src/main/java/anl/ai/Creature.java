package anl.ai;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import anl.Effect;
import anl.Inventory;
import anl.Spell;
import anl.Tile;
import anl.World;
import anl.item.Armor;
import anl.item.Bread;
import anl.item.Can;
import anl.item.Drug;
import anl.item.Food;
import anl.item.Fruit;
import anl.item.Item;
import anl.item.Medicine;
import anl.item.Weapon;
import anl.outil.Constants;
import anl.outil.Point;
import anl.screens.BackpackScreen;
import anl.screens.Screen;
import asciiPanel.AsciiPanel;

public class Creature {

	private World world;

	public int x;
	public int y;
	public int z;

	private char glyph;

	public char glyph() {
		return glyph;
	}

	private Color color;

	public Color color() {
		return color;
	}

	private CreatureAi ai;

	public void setCreatureAi(CreatureAi ai) {
		this.ai = ai;
	}

	private int maxHp;

	public int maxHp() {
		return maxHp;
	}

	public void modifyMaxHp(int amount) {
		maxHp += amount;
	}

	private int hp;

	public int hp() {
		return hp;
	}

	private int attackValue;

	public void modifyAttackValue(int value) {
		attackValue += value;
	}

	public int attackValue() {
		return attackValue + (weapon == null ? 0 : weapon.attackValue());
	}

	private int defenseValue;

	public void modifyDefenseValue(int value) {
		defenseValue += value;
	}

	public int defenseValue() {
		return defenseValue + (armor == null ? 0 : armor.defenseValue());
	}

	private int visionRadius;

	public void modifyVisionRadius(int value) {
		visionRadius += value;
	}

	public int visionRadius() {
		return visionRadius;
	}

	private String name;

	public String name() {
		return name;
	}

	private Inventory inventory;

	public Inventory inventory() {
		return inventory;
	}

	private int maxFood;

	public int maxFood() {
		return maxFood;
	}

	private int food;

	public int food() {
		return food;
	}

	private Weapon weapon; // 1 weapon

	public Weapon weapon() {
		return weapon;
	}

	private Armor armor; // 1 armor

	public Armor armor() {
		return armor;
	}

	private int xp;

	public int xp() {
		return xp;
	}

	public void modifyXp(int amount) {
		xp += amount;

		notify("You %s %d xp.", amount < 0 ? "lose" : "gain", amount);

		while (xp > (int) (Math.pow(level, 1.75) * 25)) {
			level++;
			doAction("advance to level %d", level);
			ai.onGainLevel();
			modifyHp(level * 2, "Died from having a negative level?");
		}
	}

	private int level;

	public int level() {
		return level;
	}

	private int regenHpCooldown;
	private int regenHpPer1000;

	public void modifyRegenHpPer1000(int amount) {
		regenHpPer1000 += amount;
	}

	private List<Effect> effects;

	public List<Effect> effects() {
		return effects;
	}

	private int maxMana;

	public int maxMana() {
		return maxMana;
	}

	public void modifyMaxMana(int amount) {
		maxMana += amount;
	}

	private int mana;

	public int mana() {
		return mana;
	}

	public void modifyMana(int amount) {
		mana = Math.max(0, Math.min(mana + amount, maxMana));
	}

	private int regenManaCooldown;
	private int regenManaPer1000;

	public void modifyRegenManaPer1000(int amount) {
		regenManaPer1000 += amount;
	}

	private String causeOfDeath;
	public String causeOfDeath() {
		return causeOfDeath;
	}
	public boolean statusFix = false;
	public int statusFixValue;
	
	private Set<Creature> neighbour8;
	public Set<Creature> getNeighbour8() {
		neighbour8();
		return neighbour8;
	}
	
	private Set<Point> ground8;
	public Set<Point> getGround8() {
		ground8();
		return ground8;
	}

	private int MEDICINE_INTERVAL = 0;
	
	private int emote = 85;
	public double getEmote() {
		return emote;
	}
	public void setEmote(int e) {
		emote += e;
	}

	public boolean DONATING = false;
	
	public Creature(World world, char glyph, Color color, String name, int maxHp, int attack, int defense) {
		this.world = world;
		this.glyph = glyph;
		this.color = color;
		this.maxHp = maxHp;
		this.hp = maxHp;
		this.attackValue = attack;
		this.defenseValue = defense;
		this.visionRadius = Constants.VISION_RADIUS;
		this.name = name;
		this.inventory = new Inventory(Constants.BACKPACK_VOLUME);
		this.maxFood = Constants.MAX_FOOD;
		this.food = maxFood / 3 * 2;
		this.level = 1;
		this.regenHpPer1000 = 10;
		this.effects = new ArrayList<Effect>();
		this.maxMana = 5;
		this.mana = maxMana;
		this.regenManaPer1000 = 20;
		this.neighbour8 = new HashSet<Creature>();
		this.ground8 = new HashSet<Point>();
	}

	

	public void moveBy(int mx, int my, int mz) {
		if (mx == 0 && my == 0 && mz == 0)
			return;

		Tile tile = world.tile(x + mx, y + my, z + mz);

		if (mz == -1) {
			if (tile == Tile.STAIRS_DOWN) {
				doAction("walk up the stairs to level %d", z + mz + 1);
			} else {
				doAction("try to go up but are stopped by the cave ceiling");
				return;
			}
		} else if (mz == 1) {
			if (tile == Tile.STAIRS_UP) {
				doAction("walk down the stairs to level %d", z + mz + 1);
			} else {
				doAction("try to go down but are stopped by the cave floor");
				return;
			}
		}

		Creature other = world.creature(x + mx, y + my, z + mz);
		//if(other!=null && other.isPlayer()) return;

		modifyFood(-1);

		if(this.glyph() == Constants.PLAYER && this.DONATING==true) {
			this.notify("I walked away.");
			this.DONATING = false;
			this.emote -= 10;
			return;
		}
		
		if (other == null)
			ai.onEnter(x + mx, y + my, z + mz, tile);
		else //if (this.glyph() == Constants.PLAYER)
			meet(other);// meet others
	}

	private void meet(Creature other) { // nothing happens between others
		Creature p = other.isPlayer()==true?other:this;
		Creature o = other.isPlayer()==true?this:other;
		
		if (p.glyph()==Constants.PLAYER&&(o.glyph() == Constants.SOLDIER||o.glyph() == Constants.PERSON||o.glyph() == Constants.REFUGEE)) {
			if(other.isPlayer()&&this.statusFix==false&&o.glyph() == Constants.SOLDIER) {
				ai.onNotify(Constants.SNIPHER);
				o.addPlayer(other);
				SoldierAi sa = (SoldierAi)(o.ai);
				sa.snipher();
				return;
			}
			if (o.statusFix == false) {
				o.statusFixValue = (int) (Math.random() * 10);
				o.statusFix = true;
			}
			switch (o.statusFixValue) {
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:  {
				if(other.isPlayer() && o.glyph() ==  Constants.REFUGEE) {
					p.notify("A refugee comes to me.");
				}
				p.getDonation(o);break;
			}
			case 7:
			case 8:			
				p.getAttacked(o);
				break;
			case 9:
			default:  p.talk(o); break;
			}
		}

	}

	private void talk(Creature other) {
		modifyFood(-2);
		ai.onNotify(Constants.TALK_WITH + other.name());
		setEmote(+1);
		if(other.glyph() == Constants.SOLDIER) {
			world.getCreatures().remove(other);
			ai.onNotify(Constants.GONE);
		}
		other.ai.FAMILIER++;
		if(other.ai.FAMILIER==5) {
			setEmote(+50);
			ai.onNotify("We had an excellent time.");
			if(other.ai.FAMILIER>=10) {
				world.getCreatures().remove(other);
				ai.onNotify(Constants.GONE);
			}
		}
	}

	private void getAttacked(Creature other) {
		modifyFood(-2);
		other.ai.attackStatus = true;
		other.addPlayer(this);
		other.ai.attack();
	}

	private void getDonation(Creature other) {
		if(other.glyph() == Constants.REFUGEE) {
			ai.onNotify("He asks for some supplies.");
			this.notify("I should check my backpack to see if there's anything.");
			this.DONATING  = true;
			other.addPlayer(this);
			return;
		}
		modifyFood(-10);
		ai.onNotify(Constants.DONATION_FROM+other.name());
		int random = (int) (Math.random() * 3);
		double r =  Math.random();
		r = r == 0.0 ? 1.0 : r;
		int num = (int) (r * Constants.DONATION_NUM) + 1;
		if(other.glyph() == Constants.PERSON) {
			num -= 2;
			if(num<=0) num=1;
		}
		if (num + inventory.getCount() > Constants.BACKPACK_VOLUME) {
			ai.onNotify(Constants.BACKPACK_FULL_DROP);
			num = Constants.BACKPACK_VOLUME - inventory.getCount();
		}
		switch (random) {
		case 1:
			this.inventory().add(new Can(), num);
			break;
		case 2:
			this.inventory().add(new Fruit(), num);
			break;
		case 3:
		default:
			this.inventory().add(new Bread(), num);
			break;
		}
		world.getCreatures().remove(other);
		ai.onNotify(Constants.GONE);
	}

	public void playerAttack() {
		if(weapon!=null) ai.onNotify(Constants.PLAYER_ATTACK+" with "+weapon.name()+".");
		else ai.onNotify(Constants.PLAYER_ATTACK);
		List<Creature> creatureAround = new ArrayList<Creature>();

		if (world.creature(x - 1, y, z) instanceof Creature)
			creatureAround.add(world.creature(x - 1, y, z));
		if (world.creature(x, y + 1, z) instanceof Creature)
			creatureAround.add(world.creature(x, y + 1, z));
		if (world.creature(x + 1, y, z) instanceof Creature)
			creatureAround.add(world.creature(x + 1, y, z));
		if (world.creature(x, y - 1, z) instanceof Creature)
			creatureAround.add(world.creature(x, y - 1, z));

		for (Creature e : creatureAround) {
			commonAttack(e, attackValue(), "attack the %s for %d damage", e.name);
			this.setEmote(-2);
			e.ai.underAttackStatus = true;
			e.addPlayer(this);
		}
	}

	private void addPlayer(Creature creature) {
		if (this.glyph() != Constants.PLAYER) {
			this.ai.setPlayer(creature);
		}

	}

	public void meleeAttack(Creature other) {
		commonAttack(other, attackValue(), "attack the %s for %d damage", other.name);
	}

	private void throwAttack(Weapon item, Creature other) {
		commonAttack(other, attackValue / 2 + item.thrownAttackValue(), "throw a %s at the %s for %d damage",
				nameOf(item), other.name);
		other.addEffect(item.quaffEffect());
	}

	public void rangedWeaponAttack(Creature other) {
		commonAttack(other, attackValue / 2 + weapon.rangedAttackValue(), "fire a %s at the %s for %d damage",
				nameOf(weapon), other.name);
	}

	private void commonAttack(Creature other, int attack, String action, Object... params) {
		modifyFood(-10);

		int amount = Math.max(0, attack - other.defenseValue());

		amount = (int) (Math.random() * amount) + 1;

		Object[] params2 = new Object[params.length + 1];
		for (int i = 0; i < params.length; i++) {
			params2[i] = params[i];
		}
		params2[params2.length - 1] = amount;

		doAction(action, params2);

		other.modifyHp(-amount, "Killed by a " + name);
		if(other.hp()<=0&&this.isPlayer()) {
			this.notify("I have to do that."); 
			this.setEmote(-30);
		}

	}

	public void modifyHp(int amount, String causeOfDeath) {
		hp += amount;
		this.causeOfDeath = causeOfDeath;

		if (hp > maxHp) {
			hp = maxHp;
		} else if (hp < 1) {
			doAction("die");
			leaveCorpse();
			world.remove(this);
		}
	}

	private void leaveCorpse() {
		Item corpse = new Item('%', color, name + " corpse", null);
//		corpse.modifyFoodValue(maxHp * 5);
		world.addAtEmptySpace(corpse, x, y, z);
		for (Item item : inventory.getItems()) {
			if (item != null)
				drop(item);
		}
	}

	public void dig(int wx, int wy, int wz) {
		modifyFood(-10);
		world.dig(wx, wy, wz);
		doAction("dig");
	}

	public void update() {
		
		ground8();
		neighbour8();
		modifyFood(-1);
		if(this.food()<200) emote -= 3;
		if(MEDICINE_INTERVAL>0) {
			MEDICINE_INTERVAL ++;
			if(MEDICINE_INTERVAL>Constants.DURATION_MEDICINE) MEDICINE_INTERVAL = 0;
		}
		regenerateHealth();
		regenerateMana();
		updateEffects();
		ai.onUpdate();
	}

	private void updateEffects() {
		List<Effect> done = new ArrayList<Effect>();

		for (Effect effect : effects) {
			effect.update(this);
			if (effect.isDone()) {
				effect.end(this);
				done.add(effect);
			}
		}

		effects.removeAll(done);
	}

	private void regenerateHealth() {
		regenHpCooldown -= regenHpPer1000;
		if (regenHpCooldown < 0) {
			if (hp < maxHp) {
				modifyHp(1, "Died from regenerating health?");
				modifyFood(-1);
			}
			regenHpCooldown += 1000;
		}
	}

	private void regenerateMana() {
		regenManaCooldown -= regenManaPer1000;
		if (regenManaCooldown < 0) {
			if (mana < maxMana) {
				modifyMana(1);
				modifyFood(-1);
			}
			regenManaCooldown += 1000;
		}
	}

	public boolean canEnter(int wx, int wy, int wz) {
		return world.tile(wx, wy, wz).isGround() && world.creature(wx, wy, wz) == null;
	}

	public void notify(String message, Object... params) {
		ai.onNotify(String.format(message, params));
	}

	public void doAction(String message, Object... params) {
//		for (Creature other : getCreaturesWhoSeeMe()){
//			if (other == this){
//				other.notify("You " + message + ".", params);
//			} else {
//				other.notify(String.format("The %s %s.", name, makeSecondPerson(message)), params);
//			}
//		}
	}

	public void doAction(Item item, String message, Object... params) {
//		if (hp < 1)
//			return;
//		
//		for (Creature other : getCreaturesWhoSeeMe()){
//			if (other == this){
//				other.notify("You " + message + ".", params);
//			} else {
//				other.notify(String.format("The %s %s.", name, makeSecondPerson(message)), params);
//			}
//			other.learnName(item);
//		}
	}

	private List<Creature> getCreaturesWhoSeeMe() {
		List<Creature> others = new ArrayList<Creature>();
		int r = Constants.VISION_RADIUS;
		for (int ox = -r; ox < r + 1; ox++) {
			for (int oy = -r; oy < r + 1; oy++) {
				if (ox * ox + oy * oy > r * r)
					continue;

				Creature other = world.creature(x + ox, y + oy, z);

				if (other == null)
					continue;

				others.add(other);
			}
		}
		return others;
	}

	private String makeSecondPerson(String text) {
		String[] words = text.split(" ");
		words[0] = words[0] + "s";

		StringBuilder builder = new StringBuilder();
		for (String word : words) {
			builder.append(" ");
			builder.append(word);
		}

		return builder.toString().trim();
	}

	public boolean canSee(int wx, int wy, int wz) {
		return (detectCreatures > 0 && world.creature(wx, wy, wz) != null || ai.canSee(wx, wy, wz));
	}

	public Tile realTile(int wx, int wy, int wz) {
		return world.tile(wx, wy, wz);
	}

	public Tile tile(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return world.tile(wx, wy, wz);
		else
			return ai.rememberedTile(wx, wy, wz);
	}

	public Creature creature(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return world.creature(wx, wy, wz);
		else
			return null;
	}

	public void pickup() {
		Item item = world.item(x, y, z);

		if (inventory.isFull() || item == null) {
			doAction("grab at the ground");
		} else {
			doAction("pickup a %s", nameOf(item));
			world.remove(x, y, z);
			inventory.add(item);
		}
	}

	public void drop(Item item) {
		if (world.addAtEmptySpace(item, x, y, z)) {
			doAction("drop a " + nameOf(item));
			inventory.remove(item);
			unequip(item);
		} else {
			notify("There's nowhere to drop the %s.", nameOf(item));
		}
	}

	public void modifyFood(int amount) {
		food += amount;

		if (food > maxFood) {
			maxFood = (maxFood + food) / 2;
			food = maxFood;
			notify("You can't belive your stomach can hold that much!");
		} else if (food < 1 && isPlayer()) {
			modifyHp(-1000, "Starved to death.");
		}
	}

	public boolean isPlayer() {
		return glyph == '@';
	}

	public void eat(Item item) {
		doAction("eat a " + nameOf(item));
		consume(item);
	}

	public void quaff(Item item) {
		doAction("quaff a " + nameOf(item));
		consume(item);
	}

	private void consume(Item item) {
		inventory.remove(item);
		if(DONATING == true) {
			emote += Constants.DONATION_BONUS_EMOTE;
			notify("He seems grateful. I'm glad I did it.");
			DONATING = false;
			return;
		}
		if(item instanceof Food) {
			if (((Food) item).foodValue() < 0)
				notify("Gross!");
			
			modifyFood(((Food) item).foodValue());
		}
		if(item instanceof Medicine) {
			if(MEDICINE_INTERVAL==0) MEDICINE_INTERVAL = 1;
			else if(MEDICINE_INTERVAL < item.getDuration()) {
				if(Math.random()<0.3) modifyHp(-100, Constants.DEATH_MEDICINE);
			}
			modifyHp(((Medicine)item).getCure(), Constants.DEATH_MEDICINE);
			if(item instanceof Drug) {
				setEmote(100);
			}else {
				setEmote(20);
			}
		}
	}

	private void addEffect(Effect effect) {
		if (effect == null)
			return;

		effect.start(this);
		effects.add(effect);
	}

	private void getRidOf(Item item) {
		unequip(item);
		inventory.remove(item);
		
	}

	private void putAt(Item item, int wx, int wy, int wz) {
		inventory.remove(item);
		unequip(item);
		world.addAtEmptySpace(item, wx, wy, wz);
	}

	public void unequip(Item item) {//update effet for get rid ofs
		if (item == null)
			return;
		if (item == armor) {
			((Armor)item).USING = false;
			if (hp > 0)
				doAction("remove a " + nameOf(item));
			
			this.defenseValue -= armor.defenseValue();
			armor = null;
		} else if (item == weapon) {
			((Weapon)item).USING = false;
			if (hp > 0)
				doAction("put away a " + nameOf(item));
			this.attackValue -= weapon.attackValue();
			weapon = null;
		}
	}

	public void equip(Weapon item) {
//		if (!inventory.contains(item)) {
//			if (inventory.isFull()) {
//				notify("Can't equip %s since you're holding too much stuff.", nameOf(item));
//				return;
//			} else {
//				world.remove(item);
//				inventory.add(item);
//			}
//		}
		if(item.USING == true) 
			{
			this.notify("I have it in my hand.");
			return;
			}

		if (item.attackValue() < 0 && item.rangedAttackValue() == 0)
			return;

		if(weapon!=null) unequip(weapon);
		doAction("wield a " + nameOf(item));
		
		weapon = item;
		addEffect(weapon.quaffEffect());	
		weapon.USING = true;
		this.attackValue += weapon.attackValue();
		
	}

	public void equip(Armor item) {
//		if (!inventory.contains(item)) {
//			if (inventory.isFull()) {
//				notify("Can't equip %s since you're holding too much stuff.", nameOf(item));
//				return;
//			} else {
//				world.remove(item);
//				inventory.add(item);
//			}
//		}
		if(item.USING == true) 
		{
		this.notify("I have worn it.");
		return;
		}
		if (item.defenseValue() == 0)
			return;

		if(armor!=null) unequip(armor);
		doAction("put on a " + nameOf(item));
		armor = item;
		this.defenseValue += armor.defenseValue();
		addEffect(((Armor) item).quaffEffect());
		armor.USING = true;
	}

	public Item item(int wx, int wy, int wz) {
		if (canSee(wx, wy, wz))
			return world.item(wx, wy, wz);
		else
			return null;
	}

	public String details() {
		return String.format("  level:%d  attack:%d  defense:%d  hp:%d", level, attackValue(), defenseValue(), hp);
	}

//	public void throwItem(Item item, int wx, int wy, int wz) {
//		Point end = new Point(x, y, 0);
//		
//		for (Point p : new Line(x, y, wx, wy)){
//			if (!realTile(p.x, p.y, z).isGround())
//				break;
//			end = p;
//		}
//		
//		wx = end.x;
//		wy = end.y;
//		
//		Creature c = creature(wx, wy, wz);
//		
//		if (c != null)
//			throwAttack(item, c);				
//		else
//			doAction("throw a %s", nameOf(item));
//		
//		if (item.quaffEffect() != null && c != null)
//			getRidOf(item);
//		else
//			putAt(item, wx, wy, wz);
//	}

	public void summon(Creature other) {
		world.add(other);
	}

	private int detectCreatures;

	public void modifyDetectCreatures(int amount) {
		detectCreatures += amount;
	}

	public void castSpell(Spell spell, int x2, int y2) {
		Creature other = creature(x2, y2, z);

		if (spell.manaCost() > mana) {
			doAction("point and mumble but nothing happens");
			return;
		} else if (other == null) {
			doAction("point and mumble at nothing");
			return;
		}

		other.addEffect(spell.effect());
		modifyMana(-spell.manaCost());
	}

	public String nameOf(Item item) {
		return ai.getName(item);
	}

	public void learnName(Item item) {
		notify("The " + item.appearance() + " is a " + item.name() + "!");
		ai.setName(item, item.name());
	}
	
	public void neighbour8(){
		this.neighbour8.clear();
		for(int mx = x-1; mx<=x+1; mx++) {
			for(int my = y-1; my<=y+1; my++) {
				if(this.creature(mx, my, this.z)!=null) {Creature e=this.creature(mx, my, this.z);
				if(!(e.x==this.x&&e.y==this.y)) {
					this.neighbour8.add(e);
				}
				}
			}
		}
	}
	
	public void ground8() {
		this.ground8.clear();
		for(int mx = x-1; mx<=x+1; mx++) {
			for(int my = y-1; my<=y+1; my++) {		
				Tile t = this.realTile(mx, my, this.z);		
				if(t.isGround()&&!(mx==this.x&&my==this.y)) {
					Point p = new Point(mx,my,this.z);
					this.ground8.add(p);
				}
				
			}
		}
		
	}

	

}
