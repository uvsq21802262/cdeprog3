package anl.outil;

public class Constants {
	public static final int WORLD_WEIGHT = 90;
	public static final int WORLD_HEIGHT = 32;
	public static final int WORLD_DEPTH = 5;
	public final static int NUM_SOLDIER = 10;
	public static final int NUM_PERSON = 60;
	public static final int NUM_REFUGEE = 15;

	public static final int VISION_RADIUS = 9;
	public final static char PLAYER = '@';
	public final static char SOLDIER = 's';
	public static final char PERSON = 'p';
	public static final char REFUGEE = 'r';
	public static final char ROCK = ',';
	public static final char FOOD = '%';
	public static final char MEDICINE = '¤';
	public static final char WEAPON = ')';
	public static final char ARMOR = '[';
	
	public static String TALK_WITH = new String("I talked a lot with a ");// Asciipanel write string less than 80 He seems kind but has nothing to share, the war has made everyone more desperate");
	public static String DONATION_FROM = new String("I received some food from the ");
	public static final String BACKPACK_FULL_DROP = new String("I cann't carry more things.");
	public static final String UNDER_ATTACK = new String("I was attacked.");
	public static final String PLAYER_ATTACK = new String("I hit hardly.");
	public static final String SNIPHER = new String("I WAS SHOT!!!");
	public static final String GONE = new String("He left.");
	public static final String ATTACK = new String("I should never go there.");
	public static final String CHASED = new String("I'm running out of time. He chases up.");
	public static final String ESCAPED = new String("That was close. Luckily I get out of it.");
	public static final String DEATH_SNIPHER = new String("A snipher killed me.");
	public static final String DEATH_MEDICINE = new String("I cann't feel anything but the drug.");
	public static final String DEATH_MENTAL = new String("I can't hold it anymore.");
	public static final int DONATION_NUM = 5;
	public static final int DONATION_BONUS_EMOTE = 15;
	
	public static final int ROCK_ATTACK_VALUE = 5;
	public static final int DAGGER_ATTACK_VALUE = 5;
	public static final int DAGGER_THROWN_ATTACK_VALUE = 5;
	public static final int STICK_ATTACK_VALUE = 20;
	public static final int STICK_THROWN_ATTACK_VALUE = 10;
	public static final int GUN_ATTACK_VALUE = 50;
	public static final int GUN_RANGE_ATTACK_VALUE = 85;
//	public static final int ROCK_ATTACK_VALUE = 5;
//	public static final int ROCK_ATTACK_VALUE = 5;
	public static final int MAX_FOOD = 1000;
	public static final int BREAD_VALUE = 300;
	public static final int APPLE_VALUE = 50;
	public static final int CAN_VALUE = 650;
	public static final int ARMOR_DEFENSE_VALUE = 60;
	public static final int BACKPACK_VOLUME = 9;
	
	public static final double PRICE_REMEDY = 100;
	public static final int DURATION_MEDICINE = 3;
	public static final double PRICE_DRUG = 1000;
	public static final double PRICE_BONDAGE = 700;

}
