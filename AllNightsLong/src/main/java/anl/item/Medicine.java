package anl.item;

import java.awt.Color;

public class Medicine extends Item {

	protected int cure;
	public int getCure() {return Math.random()<ratio ? cure: 0;};
	
	protected double ratio;
	public double getRatio() { return ratio;}
	
	public Medicine(char glyph, Color color, String name, String appearance) {
		super(glyph, color, name, appearance);
	}
	
}
