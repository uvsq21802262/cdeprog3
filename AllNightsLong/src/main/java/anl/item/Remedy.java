package anl.item;

import java.awt.Color;

import anl.outil.Constants;
import asciiPanel.AsciiPanel;

public class Remedy extends Medicine {

	public Remedy() {
		super(Constants.MEDICINE, AsciiPanel.brightCyan, "Remedy", null);
		price = Constants.PRICE_REMEDY;
		cure = 10;
		ratio = 0.6 + Math.random()%0.4;
		duration = Constants.DURATION_MEDICINE; //interval
	}
	
	public Remedy(char glyph, Color color, String name, String appearance) {
		super(glyph, color, name, appearance);
		price = Constants.PRICE_REMEDY;
		cure = 10;
		ratio = 0.6 + Math.random()%0.4;
		duration = Constants.DURATION_MEDICINE; //interval
	}

}
