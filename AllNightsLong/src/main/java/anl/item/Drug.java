package anl.item;

import java.awt.Color;

import anl.outil.Constants;
import asciiPanel.AsciiPanel;

public class Drug extends Medicine {

	public Drug() {
		super(Constants.MEDICINE, AsciiPanel.blue, "Drug", null);
		price = Constants.PRICE_DRUG;
		cure = 60;
		ratio = 0.7 + Math.random()%0.2;
		duration = Constants.DURATION_MEDICINE + 5; //interval
	}

}
