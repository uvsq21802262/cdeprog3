package anl.item;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

public class Item {

	private char glyph;
	public char glyph() { return glyph; }
	
	private Color color;
	public Color color() { return color; }

	private String name;
	public String name() { return name; }

	private String appearance;
	public String appearance() { return appearance; }
	
	protected double price;
	public double getPrice() { return price; }
	
	protected int duration;
	public int getDuration() { return duration;} 
	
	public Item(char glyph, Color color, String name, String appearance){
		this.glyph = glyph;
		this.color = color;
		this.name = name;
		this.appearance = appearance == null ? name : appearance;
	}
	
	public String details() {
		String details = "";
		
		return details;
	}
}
