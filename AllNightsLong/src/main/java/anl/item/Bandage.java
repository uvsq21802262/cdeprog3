package anl.item;

import java.awt.Color;

import anl.outil.Constants;
import asciiPanel.AsciiPanel;

public class Bandage extends Medicine {

	public Bandage(char glyph, Color color, String name, String appearance) {
		super(glyph, color, name, appearance);
		price = Constants.PRICE_BONDAGE;
		cure = 70;
		ratio = 0.3 + Math.random()%0.7;
		duration = 0; //interval
	}
	
	public Bandage() {
		super(Constants.MEDICINE, AsciiPanel.white, "Bandage", null);
		price = Constants.PRICE_BONDAGE;
		cure = 70;
		ratio = 0.3 + Math.random()%0.7;
		duration = 0; //interval
	}

}
