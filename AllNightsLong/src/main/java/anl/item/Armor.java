package anl.item;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;

import anl.Effect;
import anl.Spell;

public class Armor extends Item {

	public boolean USING;
	
	private int defenseValue;
	
	public int defenseValue() { return defenseValue; }
	public void modifyDefenseValue(int amount) { defenseValue += amount; }

	
	private Effect quaffEffect;
	public Effect quaffEffect() { return quaffEffect; }
	public void setQuaffEffect(Effect effect) { this.quaffEffect = effect; }
	
	private List<Spell> writtenSpells;
	public List<Spell> writtenSpells() { return writtenSpells; }
	
	public void addWrittenSpell(String name, int manaCost, Effect effect){
		writtenSpells.add(new Spell(name, manaCost, effect));
	}
	
	public Armor(char glyph, Color color, String name, String appearance, int defenseValue){
		super(glyph, color, name, appearance);
		this.defenseValue = defenseValue;
	}
	
	public String details() {
		String details = "";
		
		if (defenseValue != 0)
			details += "  defense:" + defenseValue;
		
		return details;
	}
}
