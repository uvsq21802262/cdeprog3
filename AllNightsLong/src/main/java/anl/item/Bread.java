package anl.item;

import java.awt.Color;

import anl.outil.Constants;
import asciiPanel.AsciiPanel;

public class Bread extends Food {

	public Bread() {
		super(Constants.FOOD, AsciiPanel.yellow, "bread", null, Constants.BREAD_VALUE);
	}

}
