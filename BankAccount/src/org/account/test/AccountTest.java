package org.account.test;

import static org.junit.Assert.*;

import org.account.*;
import org.junit.Before;
import org.junit.Test;

public class AccountTest {
	static Account a;
	static Account b;
	static Account c = null;
	
	
	/**
	 * @throws Exception: when the creation of an account fail.
	 */
	@Before
	public void setUp() throws Exception {
		a = new Account(2);
		b = new Account(1);
	}
	
	
	/**
	 * @throws IllegalAmountException: When opening a new account with negative amount.
	 */
	@Test(expected=IllegalAmountException.class)
	public void testIllegalAmountInit() throws IllegalAmountException {
		new Account(-2);
	}
	
	/**
	 * Consulting the amount of balance.
	 */
	@Test
	public void testInit() {
		assertEquals(a.consult(),2);
	}
	
	
	/**
	 * @throws IllegalAmountException: when the value is negative. 
	 */
	@Test(expected=IllegalAmountException.class)
	public void testIllegalAmountDeposit() throws IllegalAmountException {
		a.deposit(-1);
	}
	
	/**
	 * Return true when the amount of balance is equal to 3
	 * else false.
	 */
	@Test
	public void testDeposit() {
		try {
			a.deposit(1);
			assertEquals(a.consult(), 3);
		} catch (IllegalAmountException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * @throws IllegalAmountException: when the value to be taken is negative.
	 */
	@Test(expected=IllegalAmountException.class)
	public void testIllegalAmountWithdraw() throws IllegalAmountException {
		try {
			a.withdraw(-1);
		} catch (OverdraftException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * @throws OverdraftException: when the value to be taken is greater than the amount in the account.
	 */
	@Test(expected=OverdraftException.class)
	public void testOverdraftWithdraw() throws OverdraftException {
		try {
			a.withdraw(3);
		} catch (IllegalAmountException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * return true when the amount to be taken is smaller than the amount in the account.
	 */
	@Test
	public void testWithdraw() {
		try {
			a.withdraw(1);
			assertEquals(a.consult(), 1);
		} catch (IllegalAmountException | OverdraftException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * @throws OverdraftException: when the amount to be transfered is greater than the amount in the account.
	 */
	@Test(expected=OverdraftException.class)
	public void testTransfer() throws OverdraftException {
		try {
			a.remit(b, 2);
			assertEquals(a.consult(), 0);
			assertEquals(b.consult(), 3);
			
			a.remit(b, 0);
			assertEquals(a.consult(), 0);
			assertEquals(b.consult(), 3);
			
			a.remit(b, 1);
		} catch (NonexistentAccountException | IllegalAmountException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * @throws NonexistentAccountException: when the account intended to receive the amount does not exist.
	 */
	@Test(expected=NonexistentAccountException.class)
	public void testVoidDestinatorTransfer() throws NonexistentAccountException {
		try {
			a.remit(c, 1);
		} catch (OverdraftException | IllegalAmountException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * @throws IllegalAmountException: when the amount to be transferred is negative.
	 */
	@Test(expected=IllegalAmountException.class)
	public void testIllegalArgumentTransfer() throws IllegalAmountException {
		try {
			a.remit(b, -1);
		} catch (NonexistentAccountException | OverdraftException e) {
			e.printStackTrace();
		}
	}
	

}
