

import org.apache.commons.math3.complex.Complex;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;


public class ComplexTest {
	private static Complex c1;
	private static Complex c2;
	
	@Before
	public void setUpBeforeClass() throws Exception {
		c1 = new Complex(2,1);
		c2 = new Complex(3,4);
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testAddComplex() {
		Complex c3 = new Complex(5,5);
		Assert.assertEquals(c3,c1.add(c2));
	}

	@Test
	public void testDivideComplex() {
		Complex c3 = new Complex(0.4,-0.2);//(a+bi)/(c+di)=(ac+bd)/(c2+d2) +((bc-ad)/(c2+d2))i
		Assert.assertEquals(c3,c1.divide(c2));
	}
	
	@Test//(expected=ArithmeticException.class) 函数处理了异常
	public void testDivideDenominatorNullException(){
		Complex c3 = new Complex(0);//(a+bi)/(c+di)=(ac+bd)/(c2+d2) +((bc-ad)/(c2+d2))i
		c1.divide(c3);
	}

	@Test
	public void testMultiplyComplex() {
		Complex c3 = new Complex(2,11);//(a+bi)*(c+di)=(ac-bd)+(bc+ad)i
		Assert.assertEquals(c3,c1.multiply(c2));
	}

	@Test
	public void testSubtractComplex() {
		Complex c3 = new Complex(-1,-3);
		Assert.assertEquals(c3,c1.subtract(c2));
	}

}
